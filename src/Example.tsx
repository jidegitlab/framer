import * as React from "react";
import { motion } from "framer-motion";

export const Example = () => (
  <motion.div animate={{ scale: 2 }} />
)

// dragConstraints={{ left: -100, right: 100 }}
// whileHover={{ scale: 1.1 }}
// whileTap={{ scale: 0.9 }} 

// <motion.div
//   animate={{
//     borderRadius: ["20%", "20%", "50%", "50%", "20%"],
//     scale: [1, 2, 2, 1, 1],
//     rotate: [0, 0, 270, 270, 0],
    
//   }}
//   transition={{ duration: 30 }}

//   />
